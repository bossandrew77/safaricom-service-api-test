$safricom Api
========

$project will solve your problem of where to start with documentation,
by providing a basic explanation of how to do it easily.



Features
--------
-rotate the array left
-Generate unique scratch number 
-Fetch scratch generated last X minutes
- 

Installation
------------

Install $project by running:

    Install Microsoft Sql Server l.e 2014
	Configure the database connection at appsettings.json
	Configure the port where Application will be running,default localhost:5252
	Open Package Manager Console and run commands to create and initialise
										enable migrations 
										update database
										
	Build/Publish the  project
	
Api calls
-------
rotate array ---api/array/rotate
create scratch Card --api/scratchcard/create
Get last created scratch card in last ---api/scratchcard/minutes/{x} x no. of minutes
Delete all inActive Records --api/ScratchCardViewModels/deleteall
Support
-------

If you are having issues, please let us know.
We have a mailing list located at: andrewmatara33@gmail.com


