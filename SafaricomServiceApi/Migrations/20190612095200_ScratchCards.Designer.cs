﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SafaricomServiceApi.Models;

namespace SafaricomServiceApi.Migrations
{
    [DbContext(typeof(scratchDbContext))]
    [Migration("20190612095200_ScratchCards")]
    partial class ScratchCards
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.1-rtm-30846");

            modelBuilder.Entity("SafaricomServiceApi.Models.ScratchCardViewModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime>("DateUpdated");

                    b.Property<DateTime>("Expiry_Date");

                    b.Property<string>("Serial_Number");

                    b.Property<int>("Status");

                    b.Property<decimal>("Voucher_Amount");

                    b.Property<string>("Voucher_Number");

                    b.HasKey("Id");

                    b.ToTable("ScratchCards");
                });
#pragma warning restore 612, 618
        }
    }
}
