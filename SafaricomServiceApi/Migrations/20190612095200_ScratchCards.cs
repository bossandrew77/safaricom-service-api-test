﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SafaricomServiceApi.Migrations
{
    public partial class ScratchCards : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ScratchCards",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Voucher_Number = table.Column<string>(nullable: true),
                    Serial_Number = table.Column<string>(nullable: true),
                    Expiry_Date = table.Column<DateTime>(nullable: false),
                    Voucher_Amount = table.Column<decimal>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScratchCards", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScratchCards");
        }
    }
}
