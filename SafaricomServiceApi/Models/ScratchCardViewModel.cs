﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace SafaricomServiceApi.Models
{
    public class ScratchCardViewModel
    {
        public int Id { get; set; }
        public string Voucher_Number { get; set; }
        public string Serial_Number { get; set; }
        public DateTime Expiry_Date { get; set; }
        public decimal Voucher_Amount { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public scratchStatus Status { get; set; }
    }

    public enum scratchStatus
    {
        taken = 0,
        purchased = 1,
        active = 2,
        inactive = 3,

    }
}
