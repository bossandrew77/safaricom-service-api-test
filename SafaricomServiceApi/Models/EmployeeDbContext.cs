﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SafaricomServiceApi.Models
{
    public class EmployeeDbContext : DbContext
    {
        public DbSet<EmployeeViewModel> Employees { get; set; }

        public EmployeeDbContext(DbContextOptions<scratchDbContext> options)
            : base(options)
        {

        }
    }
}
