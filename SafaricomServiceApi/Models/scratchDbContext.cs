﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SafaricomServiceApi.Models
{
    public class scratchDbContext : DbContext
    {
        public DbSet<ScratchCardViewModel> ScratchCards { get; set; }
        public scratchDbContext(DbContextOptions<scratchDbContext> options)
            : base(options)
        {

        }
    }
}
