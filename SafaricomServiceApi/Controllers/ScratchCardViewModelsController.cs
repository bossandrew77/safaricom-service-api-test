﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SafaricomServiceApi.Models;

namespace SafaricomServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScratchCardViewModelsController : ControllerBase
    {
        private readonly scratchDbContext _context;

        public ScratchCardViewModelsController(scratchDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Generates Unique code Number
        /// </summary>
        /// <returns></returns>
        public string UniqueCardGenerator()
        {
            int length = 15;
            const string valid = "1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            rnd.NextBytes(getSalt());

            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            if (!this.CheckScratchCardExist(res.ToString()))
            {
                return UniqueCardGenerator();
            }
            return res.ToString();
        }
        /// <summary>
        /// Generate salt
        /// </summary>
        /// <returns></returns>
        private byte[]  getSalt()
        {
            var random = new RNGCryptoServiceProvider();

            // Maximum length of salt
            int max_length = 32;
         
            // Empty salt array
            byte[] salt = new byte[max_length];

            // Build the random bytes
            random.GetNonZeroBytes(salt);
            return salt;
            // Return the string encoded salt
          
        }
        /// <summary>
        /// Create Scratch Card Record
        /// </summary>
        /// <param name="scratchCardViewModel"></param>
        /// <returns></returns>
        [HttpGet("create")]
        public async Task<IActionResult> CreateScratchCard()
        {

            var scratchCard = new ScratchCardViewModel();
            scratchCard.Voucher_Amount = 100;
            scratchCard.Status = scratchStatus.active;
            scratchCard.Voucher_Number = this.UniqueCardGenerator();
            scratchCard.Serial_Number =new  Random().Next().ToString();
            scratchCard.DateCreated = DateTime.Now;
            scratchCard.Expiry_Date = DateTime.Now.AddMonths(12);
            _context.ScratchCards.Add(scratchCard);
            await _context.SaveChangesAsync();
            return CreatedAtAction("GetScratchCardViewModel", new { id = scratchCard.Id }, scratchCard);
        }
        /// <summary>
        /// Check if scratch Card Number Exist in the Database
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [HttpGet("exist")]
        public  Boolean CheckScratchCardExist(String number)
        {
            var scratchCardViewModel = _context.ScratchCards.FindAsync(number);

            if (scratchCardViewModel == null)
            {
                return false;
            }

            return true;
        }
             /// <summary>
             /// get scratch by Serial Number
             /// </summary>
             /// <param name="Serial_Number"></param>
             /// <returns></returns>
        public bool ScratchCardFindbySerialNumber(String Serial_Number)
        {
            var obj= _context.ScratchCards.Select(e => e.Serial_Number == Serial_Number);
            if (obj!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // GET: api/ScratchCardViewModels/all
        [HttpGet]
        public IEnumerable<ScratchCardViewModel> GetScratchCards()
        {
            return _context.ScratchCards;
        }
        /// <summary>
        /// Get latest created scratch Cards by Time 
        /// </summary>
        /// <param name="minutes"></param>
        /// <returns></returns>
        public Object GetScratchCardsinsertedbyMinutes(int minutes=3)
        {
            System.TimeSpan tSpan = new System.TimeSpan(0, 0, minutes, 0);

            System.DateTime result = DateTime.Now - tSpan;
          
            return _context.ScratchCards.Select(x=>x.DateCreated >= result);
        }
        // DELETE: Inactive Records 
        // DELETE: api/ScratchCardViewModels/deleteall
        public async Task<IActionResult> DeleteInActiveRecords()
        {
          
            var scratchCardViewModel =  _context.ScratchCards.Where(x=>x.Status== scratchStatus.inactive);
            _context.ScratchCards.RemoveRange(scratchCardViewModel); 
            await _context.SaveChangesAsync();
            return Ok(scratchCardViewModel);
        }

        // GET: api/ScratchCardViewModels/5
        /// <summary>
        /// Get get scratch Card by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetScratchCardViewModel([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var scratchCardViewModel = await _context.ScratchCards.FindAsync(id);

            if (scratchCardViewModel == null)
            {
                return NotFound();
            }

            return Ok(scratchCardViewModel);
        }
        /// <summary>
        /// Update Record By serial Number
        /// </summary>
        /// <param name="serial_no"></param>
        /// <param name="scratchCardViewModel"></param>
        /// <returns></returns>

        // Update: api/ScratchCardViewModels/5
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateScratchCardViewModel([FromRoute] string serial_no, [FromBody] ScratchCardViewModel scratchCardViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (serial_no != scratchCardViewModel.Serial_Number)
            {
                return BadRequest();
            }
            scratchCardViewModel.Status = scratchStatus.taken;
            _context.Entry(scratchCardViewModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScratchCardFindbySerialNumber(serial_no))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
    }
}