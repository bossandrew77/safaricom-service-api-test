﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SafaricomServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArrayController : ControllerBase
    {

        public  static T[] rotLeft<T>(T[] array, int count = 2)
        {
            count = -count;
            //if (count < 0)
            //    throw new ArgumentOutOfRangeException("count");
            //if (count == 0)
            //    return null;

            // If (count == array.Length) there is nothing to do.
            // So we need the remainder (count % array.Length):
            count %= array.Length;

            // Create a temp array to store the tail of the source array
            T[] tmp = new T[count];

            // Copy tail of the source array to the temp array
            Array.Copy(array, array.Length - count, tmp, 0, count);

            // Shift elements right in the source array
            Array.Copy(array, 0, array, count, array.Length - count);

            // Copy saved tail to the head of the source array
            Array.Copy(tmp, array, count);
            return array;
        }
    }
}