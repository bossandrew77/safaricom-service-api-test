﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SafaricomServiceApi.Models;

namespace SafaricomServiceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly EmployeeDbContext _context;

        public EmployeesController(EmployeeDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Getting all employees from restful and write them to console
        /// </summary>
        /// get: api/employee/all
        /// <returns></returns>
        [HttpGet("all")]
        public Object getEmployees()
        {
             const string URL = "http://dummy.restapiexample.com/api/v1/employees";
              string DATA = @"";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = "Get";
            request.ContentType = "application/json";
            request.ContentLength = DATA.Length;
            using (Stream webStream = request.GetRequestStream())
            using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
            {
                requestWriter.Write(DATA);
            }

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                    Console.Out.WriteLine(response);
                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }
            return DATA;

        }
        /// <summary>
        /// Save Employeee whose salary more than 2000 and age more than 25
        /// </summary>
        ///   Save: api/employee/save
        [HttpGet("save")]
        public void saveEmployees()
    {
        const string URL = "http://dummy.restapiexample.com/api/v1/employees";
        string DATA = @"";
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
        request.Method = "Get";
        request.ContentType = "application/json";
        request.ContentLength = DATA.Length;
        using (Stream webStream = request.GetRequestStream())
        using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
        {
            requestWriter.Write(DATA);
        }

        try
        {
            WebResponse webResponse = request.GetResponse();
            using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
            using (StreamReader responseReader = new StreamReader(webStream))
            {
                    String [] array1 = new string[5];
                    string response = responseReader.ReadToEnd();
                    array1= response.Split(",");
                    if (Convert.ToDecimal(array1[3])>1000 && Convert.ToDecimal(array1[4]) >25)
                    {
                        var employee = new EmployeeViewModel();
                        employee.Name = array1[1];
                        employee.Salary = Convert.ToDecimal(array1[2]);
                        employee.Age = Convert.ToInt32(array1[3]);
                        _context.Employees.Add(employee);
                        _context.SaveChangesAsync();
                        string something = "Created A record";

                        //Write to a file
                        using (StreamWriter writer = new StreamWriter("Wfile.txt"))
                        {
                            writer.WriteLine(something);
                        }

                    }
                Console.Out.WriteLine(response);
            }
        }
        catch (Exception e)
        {
            Console.Out.WriteLine("-----------------");
            Console.Out.WriteLine(e.Message);
        }

    }

        // Update: api/employee/update/5
        [HttpPut("update/{id}")]
        [HttpGet("/update/{id}")]
        public async Task<IActionResult> UpdateEmployeeViewModel([FromRoute] int id, [FromBody] EmployeeViewModel employeeViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employeeViewModel.Id)
            {
                return BadRequest();
            }
            employeeViewModel.Age = 40;
            _context.Entry(employeeViewModel).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!employeeViewModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        private bool employeeViewModelExists(int id)
        {
            return _context.Employees.Any(e => e.Id == id);
        }
    }
       
}
